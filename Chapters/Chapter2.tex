% BACKGROUND

\chapter{Background} % Main chapter title

\label{Chapter2} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	CHAPTER OVERVIEW
%----------------------------------------------------------------------------------------

\textit{The focus of this chapter is to provide contextual background to the path planning problem and establish a starting point from existing literature to achieve the Aims set out in Chapter \ref{Chapter1}. Section \ref{sec:motivation} of this chapter will explore in depth the motivation and benefits of developing a path planning system for forklifts. In Section \ref{sec:current forklift use} the current state of forklift use in warehouses will be discussed including the types of forklifts used and their use cases. Section \ref{sec:lit review} will summarise the existing literature relating to forklift physics and path planning that the rest of the thesis builds upon.}

%----------------------------------------------------------------------------------------
%	SECTION 1: MOTIVATION
%----------------------------------------------------------------------------------------

\section{Motivation}
\label{sec:motivation}
% Prevalence of forklifts in industry - wide reaching problem
In the last century, manual handling of materials in industry has been almost entirely replaced by mechanised systems of lifting and transporting loads. Several such mechanised systems find significant use, including conveyor belts and cranes, but perhaps the most important and prevalent of these is the forklift. Industry today is highly dependent on the efficiency of moving materials in the supply chain and the forklift is present at almost every link of this chain, providing an adaptable and universal micro transport solution that connects the larger road, rail, sea, and air links. According to World Industrial Truck Statistics (WITS), there were more than 1.18 million orders placed worldwide for forklift type vehicles in 2016, and almost 20,000 in Oceania \cite{WITS}. In addition, standardised pallet sizes, the fact that most warehouses are designed specifically to accommodate forklifts, and the existence of specific forklift legislation all show just how universal the forklift has become.  Improving the forklift then will have wide reaching and significant benefits across the globe.

% Benefits of increasing efficiency
In particular, a major area of research is in improving the operational efficiency of forklifts. This thesis will focus on forklift use within warehouses since this is the most common environment that they operate within. One of the major metrics that measures warehouse efficiency is product turnover time, the time that it takes for a product to be sent out of a warehouse to a customer after it has arrived. It involves time taken to unload and record a product, time taken organising and storing a delivery, and order processing time. Delays in any of these steps significantly impacts the efficiency of the business since it will hold up other processes in the warehouse \cite{AdaptaliftHyster}. Therefore in a fully autonomous forklift setting, the path that a forklift should follow should be one that minimises the product turnover time by performing product unloading, storing, and retrieval in as little time as possible. The same path could be used in a non-autonomous or semi-autonomous setting as a tool that minimises product turnover time by providing guided navigation to drivers.

% Necessity of safety 
Another important factor is safety. There are obvious ethical reasons for this, but just as significant is the financial  motivation. Forklift accidents in the US cost employers an average of \$48,000 USD in worker compensation claims for each work-related disabling injury, and an average of \$1,390,000 USD for each fatality\cite{Seegrid}. There is also significant loss of product, equipment, and productivity associated with a major forklift accident meaning a warehouse accident could quickly erase any cost savings made from advantageous planned paths. Therefore planned forklift paths must be able to guarantee avoidance of obstacles and be as safe as possible.  

% Summarising
Optimal paths then, should allow forklifts to complete tasks in as little time as possible, decreasing product turnover time and, in turn, decreasing costs and leading to increased business profits. Maintaining safety and avoiding obstacles is also an essential requirement of any planned paths. It is hard to quantify the total economic benefit of improving forklift paths, but the prevalence of forklifts globally in industry adds weight to the need for further work in this area. The question remains though as to how to find these optimal paths systematically. This is where the development of an optimal path planning system is required.

% Gap in the path planning literature
Several path planning algorithms have already been developed, primarily in the domain of autonomous cars (these algorithms are discussed in Section \ref{sec:lit review}), but these algorithms largely haven't yet been applied to forklifts in the context of warehouse materials handling. Our work will focus on using and building upon the existing path planning algorithms in this setting to develop an optimal path planning system for forklifts.

%----------------------------------------------------------------------------------------
%	SECTION 2: CURRENT STATE OF FORKLIFT USE IN WAREHOUSES
%----------------------------------------------------------------------------------------

\section{Current State of Forklift Use in Warehouses}
\label{sec:current forklift use}
% Different types of forklifts used
There are several types of powered industrial trucks used currently in industry that may be broadly referred to as forklifts. They range from small hand operated vehicles to large freight handling vehicles, and can be driven electrically or by combustion \cite{OSHA}. The different classes of powered industrial trucks are described in Table \ref{tab:forklift classes} and shown in Figure \ref{fig:forklift classes}. \\

\begin{table}[h]
	\centering
	\begin{tabular}{lll}
		Class I   & Electric Motor Rider Trucks                             & Figure \ref{fig:forklift classes}(a) \\
		Class II  & Electric Motor Narrow Aisle Trucks                      & Figure \ref{fig:forklift classes}(b) \\
		Class III & Electric Motor Hand Trucks                              & Figure \ref{fig:forklift classes}(c) \\
		Class IV  & Internal Combustion Engine Trucks (Solid/Cushion Tires) & Figure \ref{fig:forklift classes}(d) \\
		Class V   & Internal Combustion Engine Trucks (Pneumatic Tires)     & Figure \ref{fig:forklift classes}(e) \\
		Class VI  & Electric and Internal Combustion Engine Tractors        & Figure \ref{fig:forklift classes}(f) \\
		Class VII & Rough Terrain Forklift Trucks                           & Figure \ref{fig:forklift classes}(g)
	\end{tabular}
	\caption{Classes of powered industrial trucks \cite{OSHA}.}
	\label{tab:forklift classes}
\end{table}

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_1.png}
		\caption{Class I}
	\end{subfigure}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_2.png}
		\caption{Class II}
	\end{subfigure}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_3.png}
		\caption{Class III}
	\end{subfigure}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_4.png}
		\caption{Class IV}
	\end{subfigure}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_5.png}
		\caption{Class V}
	\end{subfigure}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_6.png}
		\caption{Class VI}
	\end{subfigure}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/forklift_class_7.png}
		\caption{Class VII}
	\end{subfigure}
	\caption{Examples of each class of powered industrial truck \cite{OSHA}.}
	\label{fig:forklift classes}
\end{figure}


% Primary use case of forklifts
% Justification of assumption that forklifts operate in known environments etc. 
Forklifts have a number of use cases, and are a part of both small businesses and large logistical operations. A common thread appears in the majority of these use cases though, with forklifts almost always used indoors in a warehouse environment, or outdoors in a concreted area just outside a warehouse. This can be seen in the physical characteristics of the forklift. The Class I, IV, and V forklifts described above have rear-wheel steering which increases manoeuvrability in tight areas, making them excellent for use in the tight spaces of between shelves in a warehouse. Though there are some forklifts with modifications for use in non warehouse environments, this setup does not lend itself to operating in rough terrain. 

Warehouse environments are usually known and have set positions for shelving and driving areas. This aids the path planning problem since a 2D map layout of a warehouse and the static obstacles within it (shelving etc.) is generally available. 

%----------------------------------------------------------------------------------------
%	SECTION 3: MAJOR ASSUMPTIONS
%----------------------------------------------------------------------------------------
\section{Major Assumptions}
For simplicity, and taking into consideration the main uses of forklifts in industry, we restrict the problem to the following domain: 

\begin{enumerate}
	\item The forklift vehicles considered are Class I, IV, and V powered industrial trucks only.
	\item The forklift vehicles considered are assumed to operate in a warehouse environment where the floor is perfectly flat and a map of the environment including the position and size of obstacles is known.
\end{enumerate}

These represent the major assumptions of the project but further assumptions will be made where appropriate throughout the thesis. Though we are restricting the domain of the problem, the work done in this thesis has the potential to be extended to other classes of vehicle, as well as other environments.

%----------------------------------------------------------------------------------------
%	SECTION 4: REVIEW OF CURRENT LITERATURE
%----------------------------------------------------------------------------------------

\section{Review of Current Literature}
\label{sec:lit review}
\subsection{Forklift Physics}
%TODO: define holonomic, non holonomic etc.
The forklift classes that we are interested in, as defined in Section \ref{sec:current forklift use}, are non-holonomic vehicles. This means that the system is subject to differential constraints such that the current state of the vehicle depends on the path taken to achieve the state. In practice, this means that the vehicle cannot turn on the spot and cannot make arbitrarily tight turns, as opposed to holonomic vehicles which have no such constraints. Any turning motion of a forklift must be accompanied by some forward or backward motion. 

This has some significant implications for path planning since not all paths will be achievable by the forklift due to the non-holonomic constraint.

In addition, the forklift has a high propensity for toppling compared to other vehicles. The forklift and its load have a constantly varying centre of gravity that changes with any movement of the load. For this reason, negotiating a sharp turn or travelling too quickly around a turn can cause a topple to occur.
% Something about forklift toppling as a concern 

\subsection{Path Planning Algorithms}
There are several existing algorithms that have been developed in the literature for vehicle path planning. They can generally be divided into two categories which we define as: 

\begin{itemize}
	\item \textbf{Type 1:} These algorithms operate at the low level. They generate a trajectory that is continuous and drive-able by a non-holonomic vehicle, taking into account the vehicle's kinematic capability such as turning radius and velocity. However, these algorithms do not take into account the presence of any obstacles. 
	\item \textbf{Type 2:} These algorithms operate at a higher level than Type 1 algorithms. The generated path consists of a discrete set of turns to navigate around obstacles in a given environment. However, these algorithms do not take into account the kinematic capability of the vehicle, and in particular, are not drive-able by non-holonomic vehicles such as the forklift. Therefore they can only be applied directly to holonomic vehicles where abrupt turns are possible. 
\end{itemize}

It is clear from the advantages and disadvantages outlined above that the two types of algorithm have different purposes. Type 1 algorithms allow us to generate a curved trajectory that the vehicle can follow from a starting to an ending position, whereas Type 2 algorithms allow us to generate a set of turns to avoid obstacles but do not contain information about how to make the turns (eg. just: turn left, go straight, then turn right). 

With this in mind, we discuss four specific algorithms here.  

\subsubsection{Dubins Path}

Dubins path is a Type 1 path planning algorithm formulated by Lester Dubins in 1957 \cite{Dubins}. It solves the problem of finding the shortest forward path in a plane with specified initial and final positions and orientations, and with the constraint that at each point in the path, the radius of curvature  should be $\geq r$, where $r$ is a positive constant. The algorithm does not give an explicit formula for the shortest path, but instead, gives a set of paths that contains the optimal path. The set is small though and contains at most 6 paths for each instance of start and end conditions. Therefore the set can easily be looked through to find the optimal solution. 

Dubins showed that the set that contains the optimal path is made up of paths with three connected straight line and maximum curvature segments. This was later shown to be consistent with Pontryagin's minimum principle \cite{Boissonat}. Each member of the set can be described by one of 6 `words', giving the set as: $\{LRL, LSL, LSR, RLR, RSR, RSL\}$ where, 

\begin{itemize}
	\item $L$ signifies travelling anticlockwise around a circle of radius $r$ (ie. tightest possible anticlockwise turn) for some distance less than $2\pi$ radians. 
	\item $R$ signifies travelling clockwise around a circle of radius $r$ (ie. tightest possible clockwise turn) for some distance less than $2\pi$ radians. 
	\item $S$ means travel straight for some distance.
\end{itemize}

Some possible Dubins trajectories are shown in Figure \ref{fig:dubins trajectories}. 

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.21\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/dubins_1.png}
		\caption{$RSL$}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/dubins_2.png}
		\caption{$RSR$}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/dubins_3.png}
		\caption{$LRL$}
	\end{subfigure}
	\caption{Examples of Dubins path trajectories. \protect\footnotemark}
	\label{fig:dubins trajectories}
\end{figure}
\footnotetext{By Salix alba - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=46816721}

Each `word' in the set must be a smooth curve that is piecewise linear or circular (with radius $r$). The length of each $L$, $S$, or $R$ segment of the curve, in units of distance for $S$ segments and units of angle for $L$ and $R$ segments, must be defined also. The number of lengths to be defined, $3$, is equal to the number of endpoint conditions so may be solved for. Note that one or more of these segments may vanish by having $0$ length. 

The Dubins algorithm is summarised in Algorithm \ref{alg:dubins} below.  

\begin{algorithm}[H]
\caption{Dubins Path construction}
\label{alg:dubins}
\begin{algorithmic}[1]
\If {$\theta_{i} = \theta_{f}$ \textbf{ and } $\angle q_{s}q_{f} = 0$}
    \State Shortest path is a straight line
\Else
    \State Construct a total of 4 circles of radius $R_{min}$, tangent to $q_{i}$ and $q_{f}$
    \ForAll{Combinations of initial and final circles}
        \If {$\bar{O_{1}O_{2}} \leq 2R_{min}$} \Comment $O_{1}$ and $O_{2}$ are the initial and final center points of the circle
            \State Check if CCC type curve is possible \Comment where CCC curves are LRL or RLR
            \State Construct circles tangent to initial and final circles
            \State Choose shortest path passing through 3 circles generated by the CCC type curve
        \EndIf
        \State Check if CSC type curve is possible \Comment where CSC are LSL, RSL, LSR, or RSR
        \State Connect a straight line, tangent to initial and final circles and continuous $\theta$
    \EndFor
    \State Choose the word that results in the shortest path
\EndIf
\State Output final shortest path
\end{algorithmic}
\end{algorithm}

Dubins path provides a computationally fast path planning algorithm with guaranteed distance optimality that is suitable for non-holonomic vehicles such as the forklift. The minimum radius of curvature $r$ in the algorithm can be set to the minimum turning radius determined by the vehicle kinematics. Therefore the resulting path will be drive-able if there are no obstacles in the way.

\subsubsection{Reeds-Shepp Path}
One of the limitations of Dubins path is that it does not consider reverse motion in its solution, a capability that is available, and commonly utilised, in forklifts in warehouses. Thus there may be a shorter drive-able path with at least one change of direction (eg. from forward to reverse) than the one given by Dubins path. The Reeds-Shepp path formulated by J.A. Reeds and L.A. Shepp in 1990 extends Dubins path by allowing for reverse motion of the vehicle \cite{Reeds}. Like Dubins path, it is a Type 1 path planning algorithm.

The algorithm has a similar formulation to Dubins path, but in this case, words built from $L^+$, $L^-$, $R^+$, $R^-$, $S^+$, and $S^-$ are considered ($L^+$ indicates a turn to the left in the forward direction, while $L^-$ indicates a turn along the same circle but in the reverse direction, etc.). The resulting curve is still required to be smooth, except for where a `cusp' is created from a change in direction (eg. forward to reverse). It is not hard to see that for some starting and ending positions and orientations, words using reverse segments result in a shorter path than the one given by the Dubins algorithm. This is illustrated in Figure \ref{fig:dubins vs reeds} where the Dubins path and Reeds-Shepp path for the same starting and ending conditions is shown.  

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/dubins_vs_reed_1.png}
		\caption{Dubins $RLR$ path}
	\end{subfigure}
	\hspace{1.5cm}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/dubins_vs_reed_2.png}
		\caption{Reeds-Shepp $R^+L^-R^+$ path}
	\end{subfigure}
	\caption{Example of Dubins path vs Reeds-Shepp path with the same starting and ending conditions.}
	\label{fig:dubins vs reeds}
\end{figure}

The addition of reversing capability in the Reeds-Shepp formulation results in there being 3, 4, or 5 segments that make up each `word' and a set of 48 words that contains the optimum, rather than 3 segments in each word and a set of 6 words to look through in the case of Dubins. Some of these words actually have 2 formulas for an actual path that corresponds to the word, but there are at most 68 formulas/potential paths to look through when finding the optimum. 

The Reeds-Shepp algorithm incorporates reverse motion and results in shorter paths than Dubins in many situations. However, this simple addition makes Reeds-Shepp significantly more complex, and it is difficult to implement as well as computationally intense when compared to the simpler forward restricted algorithm. It does however have obvious advantages for forklifts in warehouses where reversing would be highly beneficial in many tight space situations, as opposed to in a car where driving forward is the predominant motion. 

\subsubsection{A* Algorithm}
The A* algorithm formalised by Hart, Nilsson, and Raphael in their 1967 paper \cite{Hart} is a Type 2 path planning algorithm when applied to the path planning problem. It is not exclusively used in this context though, and applies to graph traversal generally. A* finds extensive use because of its accuracy and performance. 

The algorithm is an extension of Dijkstra's algorithm \cite{Dijkstra} which is a best-first search that finds the shortest path between two nodes in a graph based on edge costs between nodes. The edge costs can represent the driving distances between nodes in the path planning setting. A* performs better computationally than Dijkstra by using heuristics to guide the search.  

Like Dijkstra, A* is formulated in terms of weighted graphs. The 2D map to be traversed is overlaid with a grid, where the centre of each grid cell is represented by a node in the graph. Then the distance between the centres of adjacent grid cells is stored as the edge cost between the corresponding nodes. A* constructs a tree of paths starting from a starting node and expanding paths one step at a time until one of the paths in the tree reaches the target node. At each iteration of the loop, the algorithm chooses which of the partial paths in the tree to expand further by choosing the path that minimises: 
\begin{equation}
	f(n) = g(n) + h(n)
\end{equation}
where $n$ is the last node on a path, $g(n)$ is the cost of the path from the start node to $n$, and $h(n)$ is a heuristic that estimates what the cost of the path from $n$ to the target node will be. The heuristic used to generate $h(n)$ can change depending on the specific implementation, but it must be `admissible' for the algorithm to remain optimal. That is, the heuristic cannot overestimate the actual cost to reach the target node, or the the algorithm may not find the actual shortest path. For instance, the heuristic might be based on the Euclidean straight line distance to the target node which is clearly admissible. 

A* typically uses a priority queue known as the open set to select the minimum cost nodes to expand at each step. At each step, the node with the lowest $f$ cost is removed from the open set and the $f$ and $g$ costs of its neighbours are updated before addi{}ng them to the open set. This process continues until the target node has a lower $f$ cost than any of the nodes in the open set or the open set becomes empty. To keep track of the sequence of moves that make up the path, each node keeps track of its predecessor. 

The algorithm can be processed even more efficiently if the heuristic function $h(n)$ obeys an additional constraint: 
\begin{equation}
	h(n_1) \leq r(n_1,n_2) + h(n_2) 
\end{equation}
for every edge $(n_1, n_2)$ where $r$ is the edge cost between node nodes $n_1$ and $n_2$ (if $h$ has this property it is called `monotone' or `consistent'). If this is true, then no node needs to be visited more than once in the algorithm. Therefore nodes visited and removed from the open set can be added to a closed set containing nodes that will not be considered in further expansions of the path. 

The algorithm is summarised in Algorithm \ref{alg:a*} below. 

\begin{algorithm}[H]
\caption{A* algorithm}
\label{alg:a*}
\begin{algorithmic}[1]
\Procedure{findPath*}{startPosition, targetPosition}
  \State $n_s \gets n(startPosition)$ \Comment Start node
  \State $n_t \gets n(targetPosition)$  \Comment Target node
  \State $O \gets \{ n_s\}$ \Comment{Open set initialised with $n_s$}
  \State $C \gets \emptyset$ \Comment{Closet set initialised empty}
	\While {O $\ne \emptyset$} 
	\State $n_c \gets n (minimum fCost)$ \Comment Current node
  \algstore{myalg}
\end{algorithmic}
\end{algorithm}

\begin{algorithm}                     
\begin{algorithmic}[H]                  % enter the algorithmic environment
\algrestore{myalg}
    \State $O \gets O $\textbackslash$ {n_c}$ \Comment Remove $n_c$ from openSet\;
    \State $C \gets C \cup {n_c}$ \Comment Add $n_c$ to closedSet\;
    \If{$n_c \equiv n_t$}
      \State \Return retracted path starting from $n_t$
    \Else
      \ForAll {$n' \in $ Reachable Neighbours} \Comment Neighbouring nodes
        \If {$n' \in C$ \textbf{or} $n'  = Obstacle$}
          \State skip $n'$
        \EndIf
          \State {$g' = g_{n_c} + $ Traversal Cost to $n'$} \Comment Traversal cost to neighbour prediction
          \If {$g' < g_{n'}$  \textbf{or} $n' \notin O$}
          \State $g_{n'} \gets g'$ \Comment Update g cost
          \State $f_{n'} \gets g' + h_{n'}$ \Comment Update h cost
          \State $b_{p_{n'}} \gets n_c$ \Comment Update backpointer to parent node i.e. $n_c$
          \If{$n' \notin O$}
            \State $O \gets O \cup \{n'\}$ 
          \EndIf
        \EndIf
      \EndFor
    \EndIf
  \EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

Since it is based on nodes that represent the centres of a discrete grid, the resulting path from A* is discrete and therefore not drive-able by a non-holonomic vehicle. Increasing the grid resolution significantly so that the path approximates a continuous curve is one possibility to obtain a drive-able path from the algorithm, but the increase in graph nodes to be searched would make this solution computationally infeasible. 

\subsubsection{Hybrid A* Algorithm}
Hybrid A* is neither a Type 1 nor Type 2 path planning algorithm. As its name suggests, it is a hybrid of the two categories that is based on the A* algorithm. The algorithm was originally developed by Dolgov, Thrun, Montemerlo, and Diebel and was motivated by their work done on an autonomous robotic vehicle that could navigate parking lots for the 2007 DARPA Urban Challenge \cite{Dolgov}. It addresses the problem of finding a continuous, smooth, and drive-able path that avoids obstacles and is still fast to compute. Other approaches to this problem such as directly formulating a non-linear optimisation problem in the space of controls or parameterised curves cannot guarantee fast convergence due to the complex optimisation.

The input to the algorithm is a grid based map of the obstacles in the environment to be navigated, a starting pose (position and orientation) of the vehicle, a target pose (position and orientation) of the vehicle, and a kinematic model of the vehicle.  A limitation is that the velocity of the vehicle is not modelled in the algorithm, rather a constant velocity is assumed and a velocity profile has to be fitted afterwards based on the curvature and proximity to obstacles of the path. 

Hybrid A* consists of two major phases. First, a heuristic search in continuous coordinates is performed to produce kinematically feasible trajectories. These trajectories are not necessarily optimal, but generally lie in the region of the optimum. Secondly, numerical optimisation techniques are employed to improve the generated trajectories, collapsing them to a local optimum that in practice is likely to be global also. 

The first phase is termed `Hybrid-state A* Search' and is the part of the algorithm that relates to the A* algorithm previously discussed. Just as in A*, the search space is discretized and a graph is created from the map grid with the centres of grid cells acting as neighbours in the graph. However unlike A*, each node/grid cell is also associated with a continuous pose of the vehicle. Initially, the starting pose of the vehicle is associated with the initial search node. From this node, the search is expanded by applying several steering actions to the vehicle pose (eg. apply a certain steering angle for a certain driving distance) and new children poses are generated from the kinematic model of the vehicle. For each of these children poses, the grid cell that they fall into is calculated. If the node associated with that grid cell is present in the A* open set, and the new cost of the node is lower than the current cost, then the continuous pose associated with the node is updated and the node is re-prioritised in the open set. This expansion then occurs again from the top-priority node from the open set, and this process is repeated until the target node is reached. The resulting path is guaranteed to be kinematically drive-able, but lies only in the region of the global optimum due to the discretization introduced in the formulation and the `pruning' of . In particular the path will often contain unnatural serves that require frequent turning, so the path is improved in the second phase.

Calculating the costs for each node is done in the same way as in the A* algorithm: 
\begin{equation}
	f(n) = g(n) + h(n)
\end{equation}
where $n$ is the last node on a path, $g(n)$ is the cost of the path from the start node to $n$, and $h(n)$ is a heuristic that estimates what the cost of the path from $n$ to the target node will be. But some specific heuristics are used for the $h(n)$ cost. One heuristic is termed `non-holonomic-without-obstacles' which computes the shortest path to the target from $n$ ignoring obstacles but taking into account the non-holonomic nature of the car (this is essentially either the Dubins or Reeds-Shepp path). The second heuristic is termed `holonomic-with-obstacles' and is somewhat the opposite of the first in that through dynamic programming it calculates the distance of the shortest path to the target from $n$ ignoring the non-holonomic nature of the car but avoiding obstacles (this is essentially equivalent to A*). In this way, the search is guided with a balance between optimising the continuous curvature of the path and finding the best route through obstacles. The larger of these two heuristic costs for a particular node is used as $h(n)$. 

The second phase of Hybrid A* smooths the path from the Hybrid-state A* Search by formulating a non-linear optimisation program on the coordinates of the vertices of the path, which is solved using conjugate-gradient descent. Then the path is non-parametrically interpolated between the vertices using another pass of conjugate-gradient descent. After interpolation, the resolution of the path discretization is improved from $\approx 0.5$m to $\approx 5-10$cm between vertices making the path suitable for smooth driving of the vehicle. In practice, the final path after these smoothing and interpolating operations is almost always the global optimum, though this is not guaranteed nor proven.

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.3\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/astarvshybrid1.png}
		\caption{A*}
	\end{subfigure}
	\hspace{1.5cm}
	\begin{subfigure}{0.3\textwidth}
		\includegraphics[width = \textwidth]{Figures/Background/astarvshybrid2.png}
		\caption{Hybrid A*}
	\end{subfigure}{}
	\caption{Comparison of A* and Hybrid A* paths \cite{Dolgov}.}
	\label{fig:a* vs hybrid a*}
\end{figure}

The result is an algorithm that has almost all of the benefits of A* in a fast to compute (generally) optimal path that avoids obstacles, while overcoming its shortcomings by producing a smooth non-holonomically drive-able path rather than a discrete one. The difference between the A* and Hybrid A* algorithms is illustrated in Figure \ref{fig:a* vs hybrid a*}.



