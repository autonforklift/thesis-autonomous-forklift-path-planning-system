% EVALUATION AND DISCUSSION

\chapter{Evaluation and Discussion} % Main chapter title

\label{Chapter5} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	CHAPTER OVERVIEW
%----------------------------------------------------------------------------------------

\textit{Up to this point, we have created a path planning algorithm and implemented it in simulation. In this chapter we test the implemented algorithm and evaluate its performance. In Section \ref{sec:testing} we test the algorithm along with some other algorithms already discussed in various scenarios. Section \ref{sec:discussion} discusses the results from the testing.}

%----------------------------------------------------------------------------------------
%	SECTION 1: TESTING AND EVALUATION
%----------------------------------------------------------------------------------------

\section{Testing and Evaluation}\label{sec:testing}
Having created the path planning algorithm, built a 3D simulation platform, and implemented the algorithm, we now test the performance of the algorithm and discuss the results. The algorithm, along with others to compare it to, will be applied to three different starting and target pose scenarios within our warehouse implementation. The results will be measured in terms of computation time, total path length, and the number of explored nodes, in addition observations of the path shape. To gain further insight, we also test several versions of our algorithm, with different modifications taken away to see how these modifications affect the results. 

In summary, the following algorithms will be applied to each scenario: 
\begin{itemize}
	\itemsep-1.4em
	\item \textbf{Euclidean Distance:} the straight line distance between the start and target positions.
	\item \textbf{Dubins Path:} the standard Dubins Path between the start and target pose. 
	\item \textbf{A*:} the standard A* algorithm.
	\item \textbf{Hybrid A*:} our algorithm but without Dubins Path augmentation or reversing ability.
	\item \textbf{Hybrid A* with Dubins Path:} our algorithm but without forklift reversing ability.
	\item \textbf{Hybrid A* with Reversing:} our algorithm but without Dubins Path augmentation.
\end{itemize}

\newpage
\subsection{Results}
Using a computer with a 2.9 GHz Intel Core i7 Quad Core processor, and 16GB of RAM, we obtained the following results.

\subsubsection{Scenario 1}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario1/euclidean.png}
		\caption{Euclidean Distance}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario1/dubins.png}
		\caption{Dubins Path}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario1/a*.png}
		\caption{A*}
	\end{subfigure}
\end{figure}

\begin{figure}[h]\ContinuedFloat
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario1/hybrida*.png}
		\caption{Hybrid A*}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario1/hybrida*_with_dubins.png}
		\caption{Hybrid A* with Dubins Path}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario1/hybrida*_with_reverse.png}
		\caption{Hybrid A* with Reversing}
	\end{subfigure}
	\caption{Scenario 1 paths.}
	\label{fig:scenario 1 paths}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Algorithm}         & \textbf{Search Time (ms)} & \textbf{Path Length (m)} & \textbf{Explored Nodes} \\ \hline
Euclidean Distance         & 0                  & 34.41                    & N/A                     \\
Dubins Path                & 6                 & 34.41                    & N/A                    \\
A*                         & 25                 & 56.22                    & 2,219                     \\
Hybrid A*                  & 689                & 53.74                    & 53,040                   \\
Hybrid A* with Dubins Path & 1383               & 54.63                    & 47,701                   \\
Hybrid A* with Reversing   & 4557               & 53.99                    & 207,903                  \\ \hline
\end{tabular}
\caption{Scenario 1 results.}
\label{tab:scenario 1 results}
\end{table}

\newpage
\subsubsection{Scenario 2}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario2/euclidean.png}
		\caption{Euclidean Distance}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario2/dubins.png}
		\caption{Dubins Path}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario2/a*.png}
		\caption{A*}
	\end{subfigure}
\end{figure}

\begin{figure}[h]\ContinuedFloat
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario2/hybrida*.png}
		\caption{Hybrid A*}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario2/hybrida*_with_dubins.png}
		\caption{Hybrid A* with Dubins Path}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario2/hybrida*_with_reverse.png}
		\caption{Hybrid A* with Reversing}
	\end{subfigure}
	\caption{Scenario 2 paths.}
	\label{fig:scenario 2 paths}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Algorithm}         & \textbf{Search Time (ms)} & \textbf{Path Length (m)} & \textbf{Explored Nodes} \\ \hline
Euclidean Distance         & 0                  & 47.34                    & N/A                     \\
Dubins Path                & 6                  & 51.50                    & N/A                    \\
A*                         & 28                 & 54.02                    & 1,989                   \\
Hybrid A*                  & 1536                & 62.79                    & 117,441                   \\
Hybrid A* with Dubins Path & 3591               & 62.79                    & 117,441                   \\
Hybrid A* with Reversing   & 6994               & 55.016                    & 300,987                  \\ \hline
\end{tabular}
\caption{Scenario 2 results.}
\label{tab:scenario 2 results}
\end{table}

\newpage
\subsubsection{Scenario 3}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario3/euclidean.png}
		\caption{Euclidean Distance}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario3/dubins.png}
		\caption{Dubins Path}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario3/a*.png}
		\caption{A*}
	\end{subfigure}
\end{figure}

\begin{figure}[h]\ContinuedFloat
	\centering
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario3/hybrida*.png}
		\caption{Hybrid A*}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario3/hybrida*_with_dubins.png}
		\caption{Hybrid A* with Dubins Path}
	\end{subfigure}
	\begin{subfigure}{0.32\textwidth}
		\includegraphics[width = \textwidth]{Figures/Evaluation/Scenario3/hybrida*_with_reverse.png}
		\caption{Hybrid A* with Reversing}
	\end{subfigure}
	\caption{Scenario 3 paths.}
	\label{fig:scenario 3 paths}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Algorithm}         & \textbf{Search Time (ms)} & \textbf{Path Length (m)} & \textbf{Explored Nodes} \\ \hline
Euclidean Distance         & 0                  & 15.46                    & N/A                     \\
Dubins Path                & 6                 & 20.77                    & N/A                    \\
A*                         & 21                  & 39.245                    & 1,547                     \\
Hybrid A*                  & 1273                & 43.843                    & 102,686                   \\
Hybrid A* with Dubins Path & 1276               & 44.78                    & 43,051                   \\
Hybrid A* with Reversing   & 4795               & 40.907                    & 233,376                  \\ \hline
\end{tabular}
\caption{Scenario 3 results.}
\label{tab:scenario 3 results}
\end{table}

%----------------------------------------------------------------------------------------
%	SECTION 2: DISCUSSION 
%----------------------------------------------------------------------------------------

\section{Discussion}\label{sec:discussion}
In the tests carried out, three versions of the modified Hybrid A* algorithm were used with different combinations of the modifications discussed in Section \ref{subsec:modifications}. The first, labelled as `Hybrid A*', has path smoothing applied, but not Dubins Path augmentation or the addition of reversing steering angles. The second, labelled as `Hybrid A* with Dubins', has path smoothing and Dubins Path augmentation, but not the addition of reversing steering angles. And the third, labelled as `Hybrid A* with Reversing', has path smoothing and the addition of reverse steering angles, but not Dubins Path augmentation. By testing the three versions of the algorithm, we can see the effect that each of the modifications have and where it is most appropriate to apply each version of the algorithm to produce the best performance. 

In the results shown above, we compare the performance of the paths returned from the three versions of the modified Hybrid A* algorithm against each other and to the Euclidean distance, the Dubins Path, and the A* path in terms of computation time, overall path length, and number of explored nodes. The non Hybrid A* based paths here are used as performance metrics to gain some baseline to compare the Hybrid A* based algorithms to. 

In terms of the general performance of the three Hybrid A* variants against the other metrics, we can see from the figures that they are able to find the route through obstacles in a similar way to A*. In Scenario 2 where a large number of obstacles need to be navigated, the Hybrid A* variants take exactly the same `lanes' between the obstacles as the A* path, except for where the non-holonomic constraint requires a different lane towards the end of the path to achieve the final orientation. We can see also quantitatively that the length of the Hybrid A* paths are very similar to the A* path in all three scenarios. This gives us confidence in the performance of the Hybrid A* variants since the A* path is guaranteed to minimise the distance travelled \cite{Hart} under the constraints. The Hybrid A* variants provide a continuous curvature drive-able path that closely follows the A* solution.  

In terms of computational time, the Hybrid A* variants take significantly longer to find the path than the other algorithms. The Euclidean Distance path is extremely simple and can be computed instantly. The Dubins Path does not take much longer, only having to search through 6 possible paths (as discussed in Chapter \ref{Chapter2}) and the search completed within 6ms regardless of scenario. The A* algorithm, which uses a grid search like Hybrid A*, takes around 20-30ms to compute whereas the Hybrid A* algorithms take around 700-6000ms depending on the variant used and the scenario. The disparity in time to compute the path, can be attributed largely to the number of nodes that Hybrid A* expands compared to A*. We can see that the Hybrid A* variants expand around 40-150 times as many nodes as A* depending on the variant and scenario. The large increase in explored nodes can be attributed to the use of a 3D grid that represents the search space rather than the 2D grid used in A*. The results is that the modified Hybrid A* algorithm computes quickly enough for the setup in this warehouse implementation taking around $1$s in most scenarios for the variants without reversing capability, which is an acceptable delay in the context of the real world. However, warehouses significantly larger than the one used here may lead to unacceptable delays waiting for the path to be returned due to the increase in the size of the grid to be searched. A possible solution in large warehouses is to decrease the discretization resolution used, and rely more heavily on the path smoothing operation to achieve smooth paths without unnecessary turns. 

Among the Hybrid A* variants, the `Hybrid A* with Reversing' algorithm take the longest to compute and explores the most nodes. In Scenario 1, this additional time does not lead to any noticeable improvement in the path since the target orientation is a simple one to achieve. On the other hand, in Scenarios 2 and 3 the reversing capability allows a shorter path to be found compared to the other variants. Therefore a tradeoff exists as to whether it is worth making the computation time significantly longer by using the algorithm with reversing capability in order to generate better performing paths. When the target orientation is not simple to achieve or when the target position is in an area heavily populated with obstacles, then the benefits of the reversing capability become more significant. However in most other scenarios, and especially where the warehouse is large and computation time becomes a significant factor, the cost of the increased search time begins to outweigh the benefit in path length. 

When comparing the modified Hybrid A* algorithm with and without Dubins Path augmentation, we can see that if a Dubins Path is found, the number of nodes explored can be significantly decreased such as in Scenario 3 where the algorithm with Dubins Path augmentation explored 43,051 nodes compared to 102,686 nodes explored with the algorithm without Dubins Path augmentation. However, even though significantly less nodes were explored, the overall search time was very similar at 1276ms and 1273ms for with and without Dubins Path augmentation respectively. The extra computation required to compute the Dubins Path every $N$ nodes appears to negate the gains made by reducing the node expansion search. Therefore, in our testing the augmentation of Dubins Path does not give us significant gains in computation time, but does still guarantee achieving the target pose exactly where Hybrid A* without Dubins augmentation cannot due to the discretization. Since there is no significant disadvantage to the Dubins Path augmentation, we find the algorithm with Dubins Path augmentation to be more beneficial than the one without it. 

In summary, in our testing of different versions of the modified Hybrid A* algorithm in various scenarios, we find that each of the variants provides paths that perform very well, achieving the performance of A* paths while remaining non-holonomically drive-able. The only significant disadvantage is an increased computation time, which is acceptable in warehouses of moderate size, but may become too restrictive in very large warehouses, though the parameters of the search can be tuned to increase the search efficiency in these cases. Among the Hybrid A* variants, the `Hybrid A* with Reversing' algorithm produces the shortest paths, but presents a large tradeoff in terms of computation time. Of the other two variants, the 'Hybrid A* with Dubins Path' algorithm provides the benefit of achieving the target pose exactly with a similar computation time to the algorithm without the Dubins Path augmentation, so is advantageous over the latter. In a real world warehouse implementation, a decision should be made as to whether the `Hybrid A* with Dubins Path' or `Hybrid A* with Reversing' should be used depending on the size and characteristics of the warehouse layout, and the tradeoffs in the algorithms discussed here. 




