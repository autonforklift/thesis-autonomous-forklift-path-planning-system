% PRODUCING A PATH PLANNING ALGORITHM FOR FORKLIFTS IN WAREHOUSES

\chapter{Producing a Path Planning Algorithm for Forklifts in Warehouses} % Main chapter title

\label{Chapter3} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	CHAPTER OVERVIEW
%----------------------------------------------------------------------------------------

\textit{This chapter focusses on the development of the forklift path planning algorithm. The algorithm builds on the previous work discussed in Chapter \ref{Chapter2}. Section \ref{sec:forklift model} establishes a working forklift kinematics model to be used in the algorithm, while Section \ref{sec:path planning algorithm} details the specifics of the algorithm and the modifications made to the algorithms from Chapter \ref{Chapter2}.
}

%----------------------------------------------------------------------------------------
%	SECTION 1: MODEL OF FORKLIFT KINEMATICS
%----------------------------------------------------------------------------------------
\section{Model of Forklift Kinematics}\label{sec:forklift model}

Based on work done by Vongchanh \cite{Vongchanh}, we can derive a kinematic model for the forklift. 

As can be seen in Chapter \ref{Chapter2}, Class I forklifts have 3 wheels (2 front, and 1 rear), while Class IV and Class V forklifts have 4 wheels. However, the axle of the rear wheels in Class IV and V forklifts is connected via a pin joint. Therefore the rear wheel setup may be modelled in the same way as the single rear wheel, forming a `reverse' tricycle-like model that describes the kinematics where the rear wheel provides the driving and steering actuation. The setup is illustrated in Figure \ref{fig:forklift model} below. 

\begin{figure}[H]
	\centering
	\includegraphics[width = 0.5\textwidth]{Figures/Algorithm/forkliftDynamics.png}
	\caption{Forklift model \cite{Vongchanh}.}
	\label{fig:forklift model}
\end{figure}

where: 
\begin{itemize}
	\itemsep-1em
	\item \boldmath$\alpha$ \ , \ \ is the steering angle
	\item \boldmath$R$ \ , \ \ is the turning radius 
	\item \boldmath$\dot{\theta}$ \ , \ \ is the angular velocity
	\item \boldmath$\dot{\alpha}$ \ , \ \ is the steering angle velocity
\end{itemize}

The turning radius, $R$, is the arc at which the frame of the forklift travels. This is illustrated in Figure \ref{fig:turning radius}. The radius $W_a$ is the minimum radius that the body of the forklift travels through when turning on the spot and is useful in collision avoidance.

\begin{figure}[H]
	\centering
	\includegraphics[width = 0.48\textwidth]{Figures/Algorithm/forkliftDynamics_2.png}
	\caption{Turning radius \cite{Vongchanh}.}
	\label{fig:turning radius}
\end{figure}

From Figure \ref{fig:forklift model}, we can see that R is the distance from $O$ to the intersection between rays cast in the perpendicular direction of the front and rear wheels,
\begin{equation*}
	R = d \tan(\delta) 
\end{equation*}
and, 
\begin{equation*}
	\delta = \frac{\pi}{2} - \alpha 
\end{equation*}

The wheel velocity $v_{w}$ is the tangential velocity calculated using the wheel radius $r_{w}$ and the angular velocity of the wheel $\omega_{w}$,
\begin{equation*}
	v_{w} = \omega_{w}r_{w} 
\end{equation*}

And the angular velocity of the forklift at the midpoint between the front wheels is,
\begin{equation*}
	\dot{\theta} = \frac{v_{w}}{R_{w}} 
\end{equation*}

The velocities in the forklift frame, $\{x_{1},y_{1},z_{1}\}$, are given by the following. Note that we assume a zero slip condition in the $y_1$ axis.
\begin{equation*}
\begin{split}
	v_{x_1} &= v_{w}\cos(\alpha) \\
	v_{y_1} &= 0
\end{split}
\end{equation*}

Converting these velocities to the inertial frame results to the following velocities,
\begin{equation*}
\begin{split}
	v_{x} &= v_{x_1}\cos(\theta) + v_{y_1}\sin(\theta)  =  v_{w}\cos(\theta)\cos(\alpha) \\
	v_{y} &= v_{x_1}\sin(\theta) + v_{y_1}\cos(\theta)  =  v_{w}\sin(\theta)\cos(\alpha) 
\end{split}
\end{equation*}

Finally, from these equations, the kinematic model for the forklift in state space form can be represented as:

\begin{equation}
	\begin{bmatrix} v_x \\ v_y \\ \dot{\theta} \\ \dot{\alpha} \end{bmatrix} = 
	\begin{bmatrix} 
		\cos(\theta)\cos(\alpha) & 0 \\
		\sin(\theta)\cos(\alpha) & 0 \\
		\frac{1}{R} & 0 \\
		0 & 1
	\end{bmatrix}
	\cdot
	\begin{bmatrix}
		v_w \\ \dot{\alpha}
	\end{bmatrix}
	\label{eq:state space}
\end{equation} 

From (\ref{eq:state space}), we can get the translational and angular velocities of the forklift from the wheel velocity and steering angle. It should be noted though that toppling behaviour has not been incorporated into the kinematic model here.

%----------------------------------------------------------------------------------------
%	SECTION 2: PATH PLANNING ALGORITHM
%----------------------------------------------------------------------------------------
\section{Path Planning Algorithm}\label{sec:path planning algorithm}

\subsection{Defining the Problem}
% Set up the problem mathematically, define forklift, vehicle, etc

The path planning problem is as follows. The inputs are the map of the warehouse with obstacles, the starting starting position and orientation of the forklift, and the target position and orientation of the forklift. Based on the inputs, the algorithm should find the optimal path to the target that avoids the obstacles and is able to be driven by a forklift vehicle. 

Optimality here can be defined as the shortest path by default, emphasising operational efficiency. However optimality can also be biased more heavily towards safety by reducing the sharpness and/or frequency of turns in the path. 

The algorithm should also be computationally efficient so that it can be practically realised within warehouses (where time efficiency is important). Therefore paths that are faster to compute are also optimal in the sense of minimising delay. 

These optimisation goals are summarised below: 
\begin{itemize}
	\item Computational efficiency - minimising the search time of the algorithm. 
	\item Distance optimality - minimising the length of the path returned. 
	\item Bias towards safety - penalising the sharpness and frequency of turns. 
\end{itemize}

It is clear that these goals often conflict, so generally a trade-off must be made in the optimisation problem. Our algorithm seeks to balance these goals as well as provide for the possibility of biasing more heavily towards any of these characteristics. 

\subsection{An Hybrid A* and Dubins Path Based Algorithm}
% Justification of why Hybrid A* 
As discussed in Chapter \ref{Chapter2} there are a number of existing path planning algorithms. Dubins Path and Reeds-Shepp Path generate continuous curves that are optimal in distance and able to be followed by non-holonomic vehicles such as the forklift, whereas A* generates the shortest discrete path that is and able to avoid obstacles. Though these algorithms are all fast to compute, and distance optimal, they are not suitable for our purposes since they are not able to provide a continuous path that avoids obstacles. 

On the other hand, Hybrid A* is an algorithm that sits somewhat in-between these, generating a continuous path able to be followed by the forklift that avoids obstacles also. It has almost all of the benefits of A* in that gives a fast to compute (generally) shortest path that avoids obstacles, while overcoming its shortcomings by producing a smooth non-holonomically drive-able path rather than a discrete one. For these reasons we use Hybrid A* as the basis of our algorithm. 

However, Hybrid A* was developed in the context of autonomous cars where toppling from sharp turns is not a significant concern. Avoiding toppling is a much larger concern though in the context of forklifts due to the nature of the forklift dynamics and the large and varying loads that forklifts transport. Therefore our algorithm shall make modifications to the Hybrid A* algorithm to adapt it to the specific context of forklifts, to balance the optimisation goals of computational efficiency, distance optimality, and safety. We also seek to ease implementation wherever the impact on the solution is negligible. 

For some of the modifications made in our modified Hybrid A* algorithm, we also require a continuous curvature algorithm that ignores obstacles. The two existing algorithms available are the Dubins Path and Reeds-Shepp Path. The algorithms have very similar outcomes, though Reeds-Shepp allows reversing in its solution which leads to shorter paths in some situations. Therefore Reeds-Shepp is the best choice for this component of the algorithm. However, Reeds-Shepp is significantly harder than Dubins path to implement. Therefore taking into account the time pressures of the project, since the characteristics of the algorithms are similar, we choose to use Dubins Path to make the implementation simpler. Dubins Path can then be replaced by Reeds-Shepp in a future project to improve results further.   

\subsection{Modifications to Hybrid A*}\label{subsec:modifications}
% What modifications did we make and why
% Eg. Change in how we did path smoothing compared to original Hybrid A* 
% Eg. Change in Heuristics used 

Here we describe the modifications to the original Hybrid A* algorithm in detail and the improvements that they achieve.  

\subsubsection{Path Smoothing}\label{subsec:path smoothing}
As discussed in Chapter \ref{Chapter2}, the initial returned path from Hybrid A* often contains unnatural and excessive steering angles, despite being drivable. This is largely due to the discretization of the nodes and steering angles that is involved in Hybrid A* formulation. To address this Dolgov et al. post processed the path with a conjugate gradient descent scheme \cite{Dolgov} that smoothed the path. However, in our testing, a simpler gradient descent scheme works equally well and is easier to implement. Another consideration is ensuring that the path post-smoothing still avoids obstacles in a systematic way. Therefore in order to generate a safe and smooth trajectory for the forklift, we modify the Hybrid A* algorithm with the following path smoothing scheme: 

\textit{Phase 1: Gradient descent smoothing}\\
The path is defined as a set of discrete points,
\begin{equation*}
	path = \{x_0, x_1, ..., x_n\}
\end{equation*}

For each point $x_i$ along the path, a simple objective is function defined, 
\begin{equation*}
	\frac{w_{d}}{2} (x_{i} - y_{i})^{2} + \frac{w_s}{2} (y_i - y_{i+1})^{2} + \frac{w_s}{2} (y_i - y_{i-1})^{2} 
\end{equation*}
where,
\begin{itemize}
	\item $x_{i}$ is the position of the original point from the Hybrid A* solution. 
	\item $y_{i}$ is the position of the point after smoothing.
	\item $w_d$ is the weighting of retaining the original point.
	\item $w_s$ is the weighting of moving the point to the smoother position.
\end{itemize}
This objective function attempts to minimise the distance between the original point and the smoothed point through the $(x_{i} - y_{i})^2$ term, as well as minimise the distance between two consecutive smoothed points through the $(y_i - y_{i-1})^2$ and $(y_i - y_{i+1})^2$ terms. Thus, the objective function attempts to smooth the path without significantly changing the original trajectory. 

Gradient descent was then used on this objective function to generate the new smoothed set of points. Note that a quadratic objective function was selected in order to have a well behaved gradient, so that it can be computed efficiently. At each update of the path smoother, the iterative gradient descent is defined as,
\begin{equation*}
	y_{i} = y_{i} + w_{d}(x_{i} - y_{i}) + w_{s}(y_{i+1} + y_{i-1} - 2y_{i}) 
\end{equation*}

\textit{Phase 2: Ensuring obstacle avoidance}\\
The first phase results in a smoothed path but does not guarantee that the paths are still collision free. This may result in some points on the path being shifted into unsafe regions of the grid where obstacles lie. Therefore, to systematically maintain obstacle avoidance the gradient descent is initially run on the Hybrid A* solution, then the smoothed path is checked for collisions with obstacles. If a collision exists in the path at point $y_i$, the original Hybrid A* solution $x_i$ is `anchored' down here since the Hybrid A* solution guarantees is known to be collision free. The gradient descent is then run again but with the constraint that the anchored points are not moved from their positions. \\

\begin{figure}[h] 
	\centering
	\includegraphics[width = 0.8\textwidth]{Figures/Algorithm/path_smoothing.png}
	\caption{Illustration of path smoothing operation with anchored points close to obstacles.}
	\label{fig:path smoothing}
\end{figure}

This results in the smoother not being able to modify the anchored coordinates in future iterations of the gradient descent. In this way, the gradient descent is run iteratively, and is guaranteed to converge since we know that there is at least one safe path in the vicinity of the Hybrid A* solution, the original one. The result is a smoother, collision free path for the Forklift to travel which increases safety by decreasing the risk of toppling from sudden turns, and improves distance optimality by removing unnecessary turns.

\subsubsection{Augmentation of Dubins Path}\label{subsec:augmentation of dubins path}
The solution from Hybrid A* itself does not account for the final orientation of the forklift at the target position. The final orientation is an important consideration since in many scenarios, the forklift needs to be oriented a certain way to be able to pick up or deposit loads in tight spaces within the warehouse. To address this, the Hybrid A* algorithm is modified by augmenting it with the Dubins Path solution, which takes into account the final orientation of the vehicle. 

It should be first noted that Dubins Path always returns the shortest drive-able path in the absence of obstacles. And Hybrid A* consistently expands grid cell nodes from the starting node until the target node is reached. The augmentation then works as follows: 
\begin{itemize}
	\item The Dubins Path for the first node is calculated. If it happens to be collision free, we return this as the solution.
	\item Otherwise, as nodes are expanded, the Dubins Path is calculated for every $N_i$th node. If the Dubins Path is collision free, the expansion is stopped. The solution path is made up of the expanded nodes up to where the expansion was stopped, and the Dubins path from this point to the target. Otherwise, the node expansion continues.
	\item As the nodes are expanded closer to the target node, when the Euclidean (straight line) distance the target from each node becomes $<R_f$, where R is some radius, then every $N_f$th node, where $N_f < N_i$, is checked for the Dubins Path. Once again if the Dubins Path from one of these nodes is found to be collision free, it makes up the remainder of the path. Otherwise the node expansion continues.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width = 0.8\textwidth]{Figures/Algorithm/dubins_augmentation.png}
	\caption{Example of Dubins Path being concatenated to the end of the Hybrid A* search.}
	\label{fig:dubins augmentation}
\end{figure}


In this way, not every expanded node is checked for a valid Dubins Path since if nodes are checked too often when they are a long way from the target, there are likely to be obstacles in the way and it can become computationally expensive. Instead, as the expanded nodes begin to approach the target position, more and more nodes are checked for a Dubins Path solution. Furthermore, incorporating Dubins Path into Hybrid A* solution in this way will actually reduce the search time as Hybrid A* does not have to expand nodes all the way to the target, significantly reducing the number of nodes that need to be expanded. 

Thus the augmentation of Dubins Path to the end of the Hybrid A* path ensures the forklift arrives at the target with the correct orientation and decreases the search time of the overall algorithm. The parameters $R_f$, $N_i$, and $N_f$ can be tuned according to the size of the warehouse and density of objects to provide the best performance. 

\subsubsection{Addition of Reversing Capability in the Hybrid A* Search}
The original Hybrid A* formulation considered only forward motion of the forklift in the node expansion search. However in some cases, the optimal path may involve reversing, therefore we add reversing capability to the search. 

To do this we expand nodes along along the set of discretized steering angles, $\{\alpha_1, \alpha_2,..., \alpha_n\}$, in both the forward and reverse directions as shown in Figure \ref{fig:reverse steering angles }. Each node in the grid then also stores a direction (forward or reverse) associated with the continuous pose of the forklift in the grid cell. 

\begin{figure}[h]
	\centering
	\includegraphics[width = 0.5\textwidth]{Figures/Algorithm/reverse_steering_angles.png}
	\caption{Use of reverse steering angles in the Hybrid A* expansion.}
	\label{fig:reverse steering angles }
\end{figure}

The addition of reversing capability allows our algorithm to generate shorter paths in situations where reversing is beneficial, such as in tight spaces. \\


\subsection{Summary of the Algorithm}
With the previous modifications defined, the path planning algorithm is summarised below:
\subsubsection{Inputs}
\begin{itemize}
	\itemsep-0.5em
	\item A static 2D map of the environment defined in $x$ and $y$ coordinates with the positions and sizes of obstacles defined in these coordinates. 
	\item A starting position and starting orientation of the forklift, $(x_i,y_i,\theta_i)$.
	\item A target position and target orientation, $(x_f, y_f, \theta_f)$.
\end{itemize}

\subsubsection{Setup}
\begin{enumerate} 
	\itemsep-0.5em
	\item Discretize the map into a 3D grid of nodes where: 
	\begin{itemize}
		\itemsep-0.7em
		\item The 3 dimensions are the $x$ direction, the $y$ direction, and the orientation $\theta$. 
		\item Each node is defined by its discrete position and orientation within the grid, but is also associated with a continuous pose (position and orientation) of the forklift within the cell, as well as the direction of the forklift (forward or reverse).
		\item Each node has an associated $f$ cost where $f = g + h$, $g$ is the length of the path from the starting position to the node, and $h$ is a heuristic cost that estimates the distance from the node to the target, the calculation of which is determined by the implemented heuristics.
		\item The final path is made up of these nodes, and each node stores the previous node in the path if applicable.
	\end{itemize}  
	\item Mark nodes as `undrive-able' if they overlap with an obstacle. 
	\item Set up an (initially empty) open set in the form of a min priority queue sorted by $f$ cost that stores candidate nodes to be examined.
	\item Set up an (initially empty) closed set of nodes that have already been examined. 
	\item Determine the node that contains the target position of the forklift and set this as the target node. 
	\item Add the node that contains the starting position of the forklift to the open set.
\end{enumerate} 

\subsubsection{Procedure}
\begin{enumerate} 
	\setcounter{enumi}{6}
	\itemsep-0.5em
	\item Get the highest priority node from the open set. If this node is the target node then we are done, retrace the path using each node's knowledge of the previous node in the path. 
	\item Otherwise, add this node to the closed set and expand from it a specified drive distance, $d$, along a set of discretized steering angles, $\{\alpha_1, \alpha_2,..., \alpha_n\}$, in both the forward and reverse directions using the forklift kinematic model.
	\item If the nodes that the expansion arrives in are drive-able and not already in the open set or closed set, add them to the open set and update their costs and associated continuous poses.
	\item Re-prioritise the nodes in the open set to maintain the min priority queue based on $f$ cost. 
	\item Loop through steps 7-10 until the target node is reached. 
\end{enumerate}

\subsubsection{Check for Dubins Path Solution}
\begin{enumerate}
	\setcounter{enumi}{11}
	\item As discussed in Section \ref{subsec:augmentation of dubins path}, to guarantee reaching the target pose exactly, after every $N$th iteration of the procedure above, look for a valid collision free Dubins Path solution. 
	\item If one is found, stop the search and construct the path by retracing the path so far using each node's knowledge of the previous node and adding the Dubins Path solution to the end.
\end{enumerate}

\subsubsection{Post Processing}
\begin{enumerate}
	\setcounter{enumi}{13}
	\itemsep-0.5em
	\item Smooth the path using the path smoothing scheme outlined in Section \ref{subsec:path smoothing}
\end{enumerate} 

\subsection{Heuristics}

Each node in the grid has an associated $f$ cost given by, 
\begin{equation*}
	f = g + h
\end{equation*}
where, 
\begin{itemize}
	\itemsep-1em
	\item $g$ is the length of the path from the starting position to the node
	\item $h$ is a heuristic cost determined by the implemented heuristics.
\end{itemize}

The algorithm works by minimising the $f$ cost of the path, so the function that determines $h$ guides the algorithm's search through the nodes by playing a part in the prioritisation of nodes in the open set. Each of these heuristics guides the search towards the target, but the nature of the algorithm means that they are relatively modular in that they can be swapped out, combined, or new ones can be added, in order to bias the search. 

Although given enough time the algorithm will eventually find the optimal path, the choice of heuristic can significantly change the computational time involved in the search. Therefore it beneficial to have a heuristic that makes the search efficient so that our algorithm can be implement in real time in a practical warehouse setting where large delays are highly undesirable. Here we discuss three of these heuristics, and the effect that they have on the search. 

\subsubsection{Euclidean Distance}
The Euclidean distance heuristic is given by: 
\begin{equation*}
	h = \sqrt{r_x^2 + r_y^2}
\end{equation*}
where $r_x$ and $r_y$ are the distances from the current node to the target node in the $x$ and $y$ directions respectively.

This is a heuristic that has the advantage of being very easy to implement and fast to compute, so it has the potential to decrease the search time of the algorithm in simple environments and scenarios. It guides the algorithm search in the general direction of the target since nodes closer to the target will have a lower $h$ cost. However this heuristic does not consider in any way the orientation of the current pose or the target pose, nor does it incorporate obstacles in its formulation. Therefore this heuristic might direct the search in a direction where an obstacle blocks the path. It might also direct the search in a direction where the search approaches the target from the wrong direction, and this can lead to significant error in the final orientation of the actual path compared to the target orientation. 

These weaknesses are addressed in the next two heuristics, but at the expense of simplicity in implementation and speed of the $h$ cost computation.  

\subsubsection{Non-Holonomic Without Obstacles}
The `non-holonomic without obstacles' heuristic is based on Dubins Path and is given by: 
\begin{equation*}
	h = \text{Dubins}\Big((x_c, y_c, \theta_c), (x_f, y_f, \theta_f)\Big)
\end{equation*}
where $(x_c, y_c, \theta_c)$ is the continuous pose associated with the current node and $(x_f, y_f, \theta_f)$ is the target pose.

The $h$ cost here then is the length of the Dubins Path from the current pose to the target pose. Since Dubins Path gives the optimal path for non-holonomic vehicles such as the forklift in the absence of obstacles and considers the starting and ending orientations, this heuristic guides the search such that it will approach the target node from the correct direction. Though this heuristic takes longer to compute the $h$ cost for each node than the Euclidean distance heuristic, it can decrease the overall search time in situations where the target orientation differs significantly from the orientation of the starting pose. The `non-holonomic without obstacles' heuristic does not consider the presence of obstacles so has the same problems as the Euclidean heuristic in potentially directing the search in a direction where an obstacle blocks the path.  

\subsubsection{Holonomic With Obstacles}
The `holonomic with obstacles' heuristic is based on the A* algorithm and is given by:  
\begin{equation*}
	h = \text{A*}(n_c, n_f, \text{node grid})
\end{equation*}
where `node grid' is the grid of nodes, $n_c$ is the current node int the grid, and $n_f$ is the target node. 

The $h$ cost here then is the length of the A* path from the current node to the target node. Being a holonomic path, this heuristic does not take into account orientations as opposed to the `non-holonomic without obstacles' heuristic and so has the same problem as the Euclidean distance heuristic in that it might direct the search in a direction where the search approaches the target pose from the wrong direction, potentially leading to a large error in the final orientation of the returned path. However, this heuristic does consider obstacles so will direct the search in a way that quickly finds the way around an obstacle, and quickly finds the gaps in a densely populated map. 

Of the three heuristics discussed here, the `holonomic with obstacles' heuristic is the most expensive in terms of time to compute the $h$ cost since the A* algorithm performs a similar node expansion search of its own, but is beneficial in reducing the overall search time in maps with many obstacles or in situations where the target pose is directly behind an obstacle or set of obstacles. 

