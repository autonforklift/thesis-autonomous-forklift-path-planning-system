% CREATING A 3D SIMULATION AND IMPLEMENTING THE PATH PLANNING ALGORITHM

\chapter{Creating a 3D Simulation and Implementing the Path Planning Algorithm} % Main chapter title

\label{Chapter4} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	CHAPTER OVERVIEW
%----------------------------------------------------------------------------------------

\textit{This chapter details the implementation of the path planning system in simulation. A discussion of the game engine used as the basis for the simulation is found in Section \ref{sec:game engine}. Then Sections \ref{sec:3D forklift environment} and \ref{sec:controlling the forklift} detail the creation of a 3D simulation platform within the game engine based on the forklift dynamics model. Section \ref{sec:algorithm implementation} concerns how we implement the path planning algorithm. And finally, the packaging of the system is detailed in Section \ref{sec:packaging} 
}

%----------------------------------------------------------------------------------------
%	SECTION 1: CHOOSING A GAME ENGINE
%----------------------------------------------------------------------------------------
\section{Choosing a Game Engine}\label{sec:game engine}
% Discuss why game engine was used to create the simulation
% Discuss different options of game engine and why we chose Unity 3D

In order to create our platform, we use a game engine to build the simulation in. Though these engines are generally created for use in the gaming industry, they are well suited to our purposes since  they allow us to be able to build the system much faster, with much of the graphics capabilities and object interactions already built in which allows us to devote more time and effort to developing the path planning algorithm itself.

A number of engines are readily available including Unreal Engine 4, Unity 3D, CryEngine3, and Blender Game Engine. Table \ref{tab:game engines} below compares their characteristics. 

\begin{table}[h]
\centering
\begin{tabular}{|l|llll|}
\hline
\textbf{Game Engine} & \textbf{Price}  & \textbf{Level of Documen-} & \textbf{Graphical} & \textbf{Primary Script} \\ 
 &  & \textbf{tation/Tutorials} & \textbf{Fidelity} & \textbf{-ing Language} \\ \hline
Unreal Engine 4      & Free            & Medium                                    & High                        & Blueprints Vis-         \\
     &           &                                     &                         & ual Scripting         \\
Unity 3D             & Free            & High                                      & Medium                      & C\#                                 \\
CryEngine3           & \$9.90USD/month & Low                                       & High                        & Lua                                 \\
Blender Game Engine  & Free            & Medium                                    & Low                         & Python                              \\ \hline                        
\end{tabular}
\caption{Comparison of available game engines.}
\label{tab:game engines}
\end{table}

The key requirements that we would like to have are low cost, a good level of documentation, and the scripting language that we are familiar with. Graphical fidelity is of less importance since we are focussed primarily on simulation ability. Therefore, Unity 3D provides the best combination of characteristics. In particular, extensive documentation and a large library of tutorials allows us to learn the engine quickly since we are not from a games development background, and the scripting language C\# which is very similar to other languages we are comfortable with. In addition, Unity 3D has built in support for a large number platforms, which allows for future implementation and integration into platforms such as mobile apps. 

%----------------------------------------------------------------------------------------
%	SECTION 2: IMPLEMENTATION ARCHITECTURE
%----------------------------------------------------------------------------------------
\section{Implementation Architecture}\label{sec:implemntation architecture}
In the following sections, the system is implemented through C\# scripts in combination with Unity 3D game objects within the Unity 3D game engine. The aim of our implementation is to mimic the potential implementation of the algorithm in a practical setting in the real world. Therefore the implementation involves first creating a simulation platform that is as accurate as possible to the real world setting before implementing the algorithm itself. 

The software can be broken up into three main layers:
\begin{enumerate}
	\itemsep-0.5em
	\item \textbf{3D Forklift and Environment:} a simulation platform that models the real world forklift and warehouse.
	\item \textbf{Forklift Controller:} a control system that allows the forklift to follow a specified path.
	\item \textbf{Path Planning Algorithm:} implementation of the algorithm that is the subject of this thesis which returns a path for the forklift to follow. 
\end{enumerate}

These layers are illustrated below in Figure \ref{fig:implementation architecture}. 

% Flow chart showing: 
% 3D environment: forklift with dynamics and warehouse
% Forklift controller: given a path (set of points), can follow it. Manual and autnomous/PID
% Path planning algorithm 

\begin{figure}[H]
	\centering
	\includegraphics[width = 0.5\textwidth]{Figures/Implementation/implementation_architecture.png}
	\caption{Software implementation architecture.}
	\label{fig:implementation architecture}
\end{figure}

%----------------------------------------------------------------------------------------
%	SECTION 3: CREATING THE 3D FORKLIFT AND ENVIRONMENT
%----------------------------------------------------------------------------------------

\section{Creating the 3D Forklift Environment}\label{sec:3D forklift environment}
The goal of the 3D forklift and environment is to create a detailed and physically accurate simulation of a forklift in a real warehouse. This environment can be used seperate from the rest of the project as a testing platform for future projects other than path planning. To do this we recreate the model of the forklift physics from Section \ref{sec:forklift model} within Unity 3D, and then create a modifiable warehouse testing environment. 

\subsection{Forklift Physics}
% Implementation details of forklift dynamics etc. 
Unity 3D has a built in 3D physics engine called PhysX which handles the physical behaviour of objects in a game. The physics engine is capable of handling acceleration, collisions, gravity, and other forces automatically. However, the built in physics engine is designed first and foremost to create realistic `looking' physics at the expense of mathematically accurate physics. In addition, the built in engine does not allow for a large degree of control over the physics calculation, with the physics engine acting much more like a somewhat unkown `black box'. This makes sense in the context of game design where completely accurate physics is not generally important, but accuracy in our simulation has much higher priority. 

Therefore, rather than relying on the built in physics to implement the forklift in simulation, we use the dynamics model derived in Section \ref{sec:forklift model} directly. Unity 3D operates on a frame update architecture. Before each frame is rendered, the calculations within the \lstinline{update()} function of a script are executed. To accomplish the physics calculation then, we place the necessary equations from the dynamics model inside the update function. The inputs to the system are the steering angle and the (forward) acceleration. Each time the update function is called, the forklift position and orientation are recalculated based on the inputs, and the graphical forklift model is rerendered in this position and orientation for each frame. 

% Psuedocode

\begin{figure}[h]
	\centering
	\includegraphics[width = 0.5\textwidth]{Figures/Implementation/forklift_in_simulation.png}
	\caption{The forklift implemented in Unity 3D.}
	\label{fig:forklift implementation}
\end{figure}

\subsubsection{Forklift Parameters}
In our implementation we use the following parameters for the dimensions of the forklift, which are based on the Hyster H80-120FT \cite{HysterH80-120FT}, a popular standard model of forklift used in industry.  

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.44\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/hyster_forklift.png}
	\end{subfigure}
	\hspace{1.5cm}
	\begin{subfigure}{0.45\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/hyster_forklift_render.png}
	\end{subfigure}
	\caption{The Hyster H80-120FT forklift \cite{MHEquipment}\cite{HysterH80-120FTImage}.}
	\label{fig:hyster forklift}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{|l|l|}
\hline 
Maximum Steering Angle           & 30$^\circ$ \\
Driving Wheel Radius             & 0.229m     \\
Wheelbase                        & 1.57m      \\
Vehicle Width                    & 1.32m      \\
Vehicle Length (including forks) & 3.9m       \\
\hline
\end{tabular}
\caption{Forklift parameters.}
\label{tab:forklift parameters}
\end{table}

These dimensions are used to instantiate the forklift kinematic model, and to make sure the forklift maintains a safe distance to obstacles. The dimensions can be modified to adapt the algorithm for use on other forklifts. 

\subsection{Warehouse}
In order for to be used in multiple settings, we would like to be able to use different warehouse setups in the system. The environment is built from a plane object, and several rectangular prism objects that represent shelves in the warehouse. These shelves can be moved around to any position within the warehouse before the simulation is run to recreate different maps. In this way the system can be put to use for the testing of any real world warehouse, with the only requirement that the warehouse layout be mapped out initially.  

\begin{figure}[h]
	\centering
	\begin{subfigure}{0.3\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/warehouse_layout_alternative1.png}
	\end{subfigure}
	\hspace{.5cm}
	\begin{subfigure}{0.3\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/warehouse_layout_alternative2.png}
	\end{subfigure}
	\hspace{.5cm}
	\begin{subfigure}{0.3\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/warehouse_layout_alternative3.png}
	\end{subfigure}
	\caption{Examples of different customized warehouse layouts.}
	\label{fig:warehouse layout examples}
\end{figure}

The specific warehouse layout that we use in our implementation and testing is shown below in Figure \ref{fig:warehouse layout}. The warehouse size is $100 \times 100$m$^2$.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\textwidth]{Figures/Implementation/warehouse_layout.png}
	\caption{Warehouse layout used in the implementation and testing.}
	\label{fig:warehouse layout}
\end{figure}

\section{Controlling the Forklift}\label{sec:controlling the forklift}
The forklift model and warehouse were implemented in the previous section, but we next need a way to control and drive the forklift. There are two cases to consider: 
\begin{enumerate}
	\itemsep-0.5em
	\item \textbf{Manual Controller:} this mimics a forklift driver in the real world by allowing manual control input such as acceleration and steering. 
	\item \textbf{Autonomous Controller:} this mimics an autonomous forklift in the real world by automatically following a given path.
\end{enumerate}

\subsection{Manual Controller}
The manual controller is implemented with the keyboard arrows dictating the input parameters. The up and down arrow keys cause the forklift model to accelerate forwards and brake respectively. The left and right arrow keys control the steering angle $\alpha$. 

This allows a user of the simulation to drive the forklift around in real time via the keyboard. This can be used to simulate the situation of a forklift driver in the real world following a planned path laid out infront of the forklift.  

\subsection{Autonomous Controller}
% Why do we need a controller instead of open loop 
It might not be obvious that any type of control system is necessary for the forklift to follow a specified path since the path generated by our algorithm contains infomation about the steering angles required to traverse between each node of the path which we could use directly. However, the aim of our 3D simulation platform is to accurately represent a real world implementation, and in this case, disturbances, modelling errors, and noise would all lead to poor performance of such an open loop approach.  Therefore we use the closed loop system shown in Figure \ref{fig:control system} which could also be implemented on a real world forklift. 

For simplicity in implementation, we assume a constant forward velocity for the forklift. Then the control system that determines the steering angle $\alpha$ is made up of two components: 
\begin{enumerate}
	\itemsep-0.5em
	\item \textbf{Cross Track Error:} a measurement of the deviation of the forklift from the specified path. 
	\item \textbf{PID Controller:} a simple PID control architecture to correct for the error.
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[width = 0.8\textwidth]{Figures/Implementation/control_system.png}
	\caption{Autonomous control system.}
	\label{fig:control system}
\end{figure}

\subsubsection{Cross Track Error (CTE)}
To implement the control system, we first define a measurement called the "cross track error" (CTE). This is an error that represents how far the forklift deviates from the path. Consider the scenario in Figure \ref{fig:CTE} of the forklift travelling between two points on the optimal path given by the algorithm.

\begin{figure}[h]
	\centering
	\includegraphics[width = 0.8\textwidth]{Figures/Implementation/CTE_v2.png}
	\caption{Cross Track Error.}
	\label{fig:CTE}
\end{figure}


From Figure \ref{fig:CTE}, we use the dot product to define the cross track error as,
\begin{equation*}
	CTE = \frac{R_z \Delta x - R_x \Delta y}{\Delta x^2 + \Delta y^2}
\end{equation*}

To measure the progress of the forklift along the trajectory, we need to calculate the dot product of the vector of the previous node P1 to the forklift (blue vector) and the vector of the previous node P1 to the next way point P2 (green vector). 


This is given by the equation below,
\begin{equation*}
	u = \frac{R_x \Delta x + R_y \Delta y}{\Delta x^2 + \Delta y^2}
\end{equation*}

Note that if u is greater than 1, this indicates that the forklift has left the segment and progressses to the next segment of the Hybrid A* trajectory (in this case if u is greater than 1 and still in the first segment, progress to the second segment).

\subsubsection{PID Controller}
With the cross track error defined, a simple PID controller is used to determine the steering angle required to follow the trajectory. Using the cross track error from the above section, the steering angle calculated for PID controller is as follows:
\begin{equation*}
	steering \ angle = -(P)(CTE) - (D)(\frac{d(CTE)}{dt}) - (I)(\sum{CTE})
\end{equation*}
where P, D and I are the proportional, derivative, and integral gains respectively. 

Note that if P is selected to be too low, the forklift would never quite reach the reference, while a high P will cause it to be unstable. If D is set too low, this will cause underdamped oscillations int the forklift when travelling the trajectory, while high derivative gains cause overdamped tracking. If I is set too low, dynamic changes in the trajectory will not correct in time, while high gains will cause the controller to go unstable. 

Therefore we tuned the PID controller until it produced optimal performance with the gains: 
\begin{equation*}
	P = 100, \ I = 100, \ D = 50
\end{equation*} 

%----------------------------------------------------------------------------------------
%	SECTION 4: IMPLEMENTING THE PATH PLANNING ALGORITHM
%----------------------------------------------------------------------------------------
\section{Implementing the Path Planning Algorithm}\label{sec:algorithm implementation}
In the previous two stages of the implementation, we created an accurate simulation platform of a forklift in a warehouse, and a method for the forklift to follow a specified path. In this stage of the implementation, we implement the algorithm from Chapter \ref{Chapter3} in the software in order to generate an optimal path for the forklift to follow.

\subsection{Architecture}
The software architecture of the path planning algorithm implementation is shown below in Figure \ref{fig:algorithm architecture}.

\begin{figure}[h]
	\centering
	\includegraphics[width = 0.7\textwidth]{Figures/Implementation/algorithm_architecture.png}
	\caption{Path planning algorithm software architecture.}
	\label{fig:algorithm architecture}
\end{figure}

We can see that the implementation is made up of the main Hybrid A* algorithm, and then the heuristics that guide the search, an implementation of a non-holonomic path algorithm (in this case Dubins Path) which is used to finish the path, and an implementation of the path smoothing scheme. We can see that the Dubins implementation is also used in the implementation of the non-holonomic without obstacles `heuristic'.

The software has been designed with modularity and best practice software modelling and design principles in mind. This can particularly be seen in the implementation of the non-holonomic path algorithm and heuristics, which are implemented with an interface so that different algorithms or heuristics can be replaced or added as required, providing the software with adaptability, easy maintenance, and the possibility of extension. 

%\subsection{Forklift Position and Orientation Prediction}

\subsection{Selecting Parameters}
In the algorithm implementation, there is a number of parameters that need to be selected and affect the performance of the algorithm in either optimality or speed of computation. They can therefore be though of as tuning parameters for the algorithm. The main parameters that need to be selected are: 
\begin{itemize}
	\itemsep-0.5em
	\item The grid size resolution, ie. the size of each grid cell/node. 
	\item The driving distance $d$ used in the node expansion of the search. 
	\item The resolution $n$ of the steering angle discretization, $\{\alpha_1, \alpha_2, ..., \alpha_n\}$, used in the node expansion of the search. 
\end{itemize}

The selection of each of these parameters in the implemented algorithm is discussed below.

\subsubsection{Grid Size Resolution}
% Grid size 1m, Justification: if you make discretization to coarse, it might not find an optimal solution when considering narrow passage ways
In tuning the grid size resolution, it is important to select an optimal value. If the grid discretization is too coarse, the probability of the algorithm failing to find a solution increases. This is because with a coarse discretization, narrow passage ways may not be recognized as a viable path by the algorithm since the larger nodes are more likely to overlap with obstacles. However, if we choose to select too fine of a discretization for the grid, this can quickly increase the computation time of the search required to find a solution to an unreasonable level, as this increases the number of nodes to expand. Considering these tradeoffs we selected a grid size resolution of $0.4\times0.4m^2$ per node. Since the grid size resolution is set as $0.4\times0.4\text{m}^2$, so we select a driving distance of $d = 0.4 \cdot \sqrt{2}$m. 

\subsubsection{Driving Distance}
For the driving distance, the displacement chosen needs to guarantee exiting the node into a neighbor node so that the node expansion of the algorithm can successfully continue. The drive distance then should be selected to be the diagonal size of a grid cell, ensuring that the node expansion always exits the cell.
\begin{equation*}
	d = \sqrt{2}\cdot(\text{grid size})
\end{equation*}

\subsubsection{Steering Angle Discretization}
The steering angle discretization determines the size of the third dimension $\theta$ of the 3D grid. Therefore discretization of steering angles used in the node expansion search represents a tradeoff between path smoothness and computation time. For instance, doubling the number $n$ of steering angles used also doubles the number of node layers in the 3D grid, quickly multiplying the size of the search space significantly. However, using too small a discretization of.

In our testing we found that using $n = 3$ steering angles was fast to compute but produced a slightly jagged path. On the other hand, increasing the number of steering angles used to $n = 11$ produced a very smooth path but was far too slow in computation time. We found that $n = 5$ steering angles was approximately the point of diminishing returns where the path did not get significantly smoother compared to the loss in computation efficiency that results. The path is not perfectly smooth, but is smooth enough so that when we pass it through our path smoothing scheme, we obtain a very smooth final path. Therefore we select the steering angle discretization used in the implementation as: 
\begin{equation*}
	n = 5 \implies  \text{steering angles} = \{\theta_1, \theta_2, \theta_3, \theta_4, \theta_5\}
\end{equation*} 

\subsection{Heuristics}
In Chapter \ref{Chapter3}, we described three heuristic functions that guide the node expansion search of the algorithm. These were the Euclidean distance heuristic, the `non-holonomic without obstacles' heuristic (utilising Dubins Path), and the `holonomic with obstacles' heuristic (utilising A*). To keep the implementation efficient, we implemented the Euclidean distance heuristic only in our simulation. 

The functionality of both Dubins Path and A* were each implemented in our code, so the framework for adding the remaining two heuristics is present, but adding these without modification slowed the search down significantly. In order to achieve an efficient implementation of these heuristics, modifications need to be made such as pre-computing the $h$ costs and storing these in a look up table that can be scaled and rotated to the position of each node rather than computing the heuristic at each iteration of the search. Unfortunately, due to project time constraints this was not able to be completed and was seen as lower priority since the algorithm performed quite well in most situations with just the Euclidean heuristic. The `non-holonomic without obstacles' heuristic and `holonomic with obstacles' heuristic become beneficial only in certain scenarios, but future and real world implementations of the algorithm should contain them for completeness. 

%----------------------------------------------------------------------------------------
%	SECTION 5: PACKAGING THE SYSTEM 
%----------------------------------------------------------------------------------------
\section{Packaging the System}\label{sec:packaging}
So far in this chapter we have described how the 3D simulation platform has been created and how the path planning algorithm hasbeen implemented. In this section we briefly summarise the efforts made towards packaging the system for use by others. This is useful so that the system we have created, though existing only in simulation, can be demonstrated and demoed by those who might be interested in industry. To do this we create visualisations that are able to show users the algorithm at work (this is also useful for testing and debugging). Secondly, we create a user interface that allows users to interact with the system. 

\subsection{Visualisations}
To aid in debugging and testing, as well as to be able to observe visually the algorithm in the simulation, we add some visualisations to the system. 

It is important that we can see the trajectory that the path has planned for the forklift. The first visualisation we added was to plot the path found by the algorithm onto the environment. Secondly, the user is able to view the node expansion of the modified Hybrid A* search, and see which nodes are in the open and closed sets that the algorithm has processed, which allows for feedback for algorithm performance. Colour change/darkness of the nodes in the visualisation is used to represent the third dimension $\theta$ of the 3D search grid. 
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/visualisation1.png}
		\caption{Planned Path}
	\end{subfigure}
	\hspace{2cm}
	\begin{subfigure}{0.25\textwidth}
		\includegraphics[width = \textwidth]{Figures/Implementation/visualisation2.png}
		\caption{Node Expansions}
	\end{subfigure}
	\caption{Algorithm visualisations.}
	\label{fig:visualisations}
\end{figure}
\subsection{UI}
To create a user-friendly system to test the algorithm we create a user interface. The UI layout has `Play' and `Reset' buttons to initiate the autonomous forklift navigation to the target location and reset its position respectively. Furthermore, the user is able to change the location of the start and end forklift poses by ticking the toggle boxes `Start Pos' and `End Pos' and is able to rotate their orientations with the directional keys. This allows the user to test multiple paths and see if the forklift can find paths and traverse different scenarios with different starting and target locations. The UI also contains a camera selector button which will allows the user to view different camera angles of the forklift as it moves throughout the environment. Finally there is an option that allows the user to control the forklift manually with the arrow keys rather than use the autonomous forklift, replicating what it would be like for a forklift driver in the real world to follow a given path. 

% Piture of the UI
