% INTRODUCTION

\chapter{Introduction} % Main chapter title

\label{Chapter1} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	CHAPTER OVERVIEW
%----------------------------------------------------------------------------------------

\textit{This chapter introduces the main topic of this thesis and outlines how the topic will be addressed. Section \ref{sec:overview} provides some brief context and an overview of the major contributions of the thesis and how they are accomplished, Section \ref{sec:aims} summarises the aims this thesis seeks to achieve, and Section \ref{sec:thesis structure} sets out the structure of the rest of the thesis.}

%----------------------------------------------------------------------------------------
%	OVERVIEW
%----------------------------------------------------------------------------------------

\section{Overview} % What, Why, How
\label{sec:overview}
Forklift vehicles are widely used in industry where they allow large and heavy loads to be handled and transported. As a vital part of most warehouse operations, their operational efficiency and safety are vital requirements. Efficient forklifts have clear economic benefits. For example, reducing the time that tasks take allows more tasks to be completed in a given time which in turn increases profitability. However forklift safety also has huge economic implications, on top of the risk to workers, since a major warehouse accident has the potential to cost a company tens of thousands of dollars. It is therefore highly desirable to be able to plan optimally efficient paths for forklifts, while avoiding obstacles and maintaining safety. Such a system can be applied in the future to fully autonomous forklifts, or used today as a planning step in manual forklifts where the system provides the driver with navigation as to the best path to drive along. 

This project will focus on developing a path planning system that finds the optimal path for forklifts in a warehouse environment while avoiding obstacles and taking into account the forklift's kinematic capability. Specifically, given access to a map of the complete layout of the warehouse, and the position of obstacles within it, the system seeks to find the shortest safe, drivable path between a starting and ending pose. 

To do this, we will first produce a path planning algorithm suitable for forklifts in warehouses based on theoretical results in the literature. We will then create a 3D simulation platform where we can implement the algorithm. Finally, we will test the implemented the algorithm and evaluate its performance. 

%----------------------------------------------------------------------------------------
%	AIMS
%----------------------------------------------------------------------------------------

\section{Aims}
\label{sec:aims}
This thesis will address the issue of safely increasing the efficiency of forklifts by focussing on the three major aims detailed below:
\begin{itemize}
	\item \textbf{Aim 1:} Produce a path planning algorithm for forklifts in warehouses. 
	\item \textbf{Aim 2:} Create a 3D forklift simulation platform and implement the planning algorithm. 
	\item \textbf{Aim 3:} Evaluate the performance of the algorithm. 
\end{itemize}

%----------------------------------------------------------------------------------------
%	THESIS STRUCTURE
%----------------------------------------------------------------------------------------

\section{Thesis Structure} 
\label{sec:thesis structure}
The thesis is set out as follows: 

Chapter \ref{Chapter2} will detail in more depth the motivation for the thesis, exploring the important benefits of developing a path planning system for forklifts. It will also briefly summarise the current state of forklift use in industry and review the current literature that applies to forklift dynamics and path planning. Each of the various existing path planning algorithms referred to throughout this thesis will be introduced, consisting of: the Dubins path, the Reeds-Shepp path, the A* algorithm, and the Hybrid A* algorithm. 

Chapters \ref{Chapter3} and \ref{Chapter4} address Aims 1 and 2 detailing the development and implementation of the path planning algorithm, as well as the 3D simulation platform. This involves deriving a model of the forklift kinematics, and developing an algorithm from the existing algorithms discussed in Chapter \ref{Chapter2}, as well as using a game engine to build the simulation platform and implement the algorithm. 

Chapter \ref{Chapter5} addresses Aim 3 where we test and evaluate how well the implemented algorithm performs and discuss the tradeoffs involved in the algorithm. 

Finally, Chapter \ref{Chapter6} summarises the thesis, discusses some interesting alternative applications, and outlines potential work that might be done to improve and extend the algorithm in the future.
